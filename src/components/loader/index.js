
import * as React from 'react';

import './styles.css'

export class Loader extends React.Component {
    render() {
        return <div className="loader">
            <div className="container">
                <div className="holder">
                    <div className="box"></div>
                </div>
                 <div className="holder">
                    <div className="box"></div>
                </div>
                 <div className="holder">
                    <div className="box"></div>
                </div>
            </div>
        </div>
    }
}
