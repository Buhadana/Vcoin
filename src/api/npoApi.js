/**
 * Created by doria on 8/24/2017.
 */
import axios from 'axios';
import config from '../config'

export const getNpos = () => {
    return axios.get(`${config.serverUrl}/npos.json`)
};