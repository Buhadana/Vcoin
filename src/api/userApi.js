/**
 * Created by doria on 8/24/2017.
 */
import axios from 'axios';
import config from '../config'

export const registerUser = (name, fbToken, imageUrl) => {
    return axios.post(`${config.serverUrl}/users.json`, {
        user: {
            name: name,
            fb_token: fbToken,
            image_url: imageUrl
        }
    });
};

export const getUser = (id) => {
    return axios.get(`${config.serverUrl}/users/${id}.json`)
};