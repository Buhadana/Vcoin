/**
 * Created by doria on 8/24/2017.
 */
import axios from 'axios';
import config from '../config'

export const getPrizes = () => {
    return axios.get(`${config.serverUrl}/prizes.json`)
};

export const buyPrize = (prizeId, userFbToken) => {
    return axios.post(`${config.serverUrl}/prizes/buy.json`, {
        prize_id: prizeId,
        user_token: userFbToken
    })
};

export const getPrizesByUser = (fbToken) => {
    return axios.get(`${config.serverUrl}/user_prizes/by_user.json?fb_token=${fbToken}`)
};