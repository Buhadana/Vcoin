/**
 * Created by doria on 8/24/2017.
 */
import axios from 'axios';
import config from '../config'

export const getVolunteerings = () => {
    return axios.get(`${config.serverUrl}/volunteerings.json`)
};

export const registerToVolunteering = (volunteeringId, userFbToken) => {
    return axios.post(`${config.serverUrl}/volunteerings/register.json`, {
        volunteering_id: volunteeringId,
        user_token: userFbToken
    })
};

export const markAsDone = (volunteeringId, userFbToken) => {
    return axios.post(`${config.serverUrl}/user_volunteerings/mark_as_done.json`, {
        volunteering_id: volunteeringId,
        user_token: userFbToken
    })
};

export const createVolunteering = ({ title, desctiption, npo_id, min_users, max_users, vcoins_cost, date, hours, location }) => {
    return axios.post(`${config.serverUrl}/volunteerings.json`, {
        volunteering: {
            title,
            desctiption,
            npo_id,
            min_users,
            max_users,
            vcoins_cost,
            date,
            hours,
            location
        }
    })
};

window.createVolunteering = createVolunteering;