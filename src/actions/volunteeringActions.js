import { registerToVolunteering, createVolunteering } from '../api/volunteeringApi'


export const register = (volunteeringId, userFbToken) => {
    let onError = error => {
        console.log(error);
    };
    return dispatch => {
        registerToVolunteering(volunteeringId, userFbToken)
            .then(result => {
                dispatch(setRegistered(result !== null));
            })
            .catch(onError);
    }
};

export const setRegistered = registered => ({
    type: 'SET_REGISTERED',
    payload: {
        registered
    }
});

export function createVolunteeringAction(volunteering) {
    return dispatch => {
        dispatch(createVolunteeringStart(volunteering));
        createVolunteering(volunteering).then((data) => {
            setTimeout(() => { dispatch(createVolunteeringSuccess(volunteering, data.data)); }, 1000)
        }, (err) => {
            dispatch(createVolunteeringFailed(volunteering, err));
        })
    }
}

function createVolunteeringStart(volunteering) {
    return {
        type: 'CREATE_VOLUNTEERING_START',
        payload: {
            volunteering
        }
    };
}

function createVolunteeringSuccess(volunteering, data) {
    return {
        type: 'CREATE_VOLUNTEERING_SUCCESS',
        payload: {
            volunteering,
            data
        }
    }
}

function createVolunteeringFailed(volunteering, error) {
    return {
        type: 'CREATE_VOLUNTEERING_ERROR',
        payload: {
            error,
            volunteering
        }
    };
}
