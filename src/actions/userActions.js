/**
 * Created by doria on 8/24/2017.
 */
import {registerUser} from '../api/userApi'

export const sendRegisterUser = (name, id, imageUrl) => {
    return dispatch => {
        registerUser(name, id, imageUrl).then((result) => {
            dispatch(receivedUser(result));
        })
    }
};

export const receivedUser = (data) => ({
    type: 'RECEIVED_USER',
    data
});

export const addVcoins = (add) => ({
    type: 'ADD_VCOINS',
    add
});