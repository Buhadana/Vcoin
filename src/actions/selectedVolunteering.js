/**
 * Created by doria on 8/25/2017.
 */
export const selectNpo = (npoId) => ({
    type: 'SELECT_NPO',
    id: npoId
});

export const selectVolunteering = (volunteeringId) => ({
    type: 'SELECT_VOLUNTEERING',
    id: volunteeringId
});