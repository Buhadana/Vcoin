import { markAsDone } from '../api/volunteeringApi'

export function markAsDoneAction(volunteerId, userFbToken) {
    return dispatch => {
        dispatch(markAsDoneStart());
        markAsDone(volunteerId, userFbToken).then((data) => {
            console.log(data);
            dispatch(markAsDoneSuccess(data.data));
        }, (err) => {
            dispatch(markAsDoneFailed(err));
        })
    }
}

function markAsDoneStart() {
    return {
        type: 'MARK_AS_DONE_START'
    };
}

function markAsDoneSuccess(prizes) {
    return {
        type: 'MARK_AS_DONE_SUCCESS',
        payload: {
            prizes
        }
    }
}

function markAsDoneFailed(error) {
    return {
        type: 'MARK_AS_DONE_ERROR',
        payload: {
            error
        }
    };
}