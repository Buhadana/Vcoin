import { getPrizes, buyPrize } from '../api/prizeApi'


export function getPrizesAction() {
    return dispatch => {
        dispatch(getPrizesStart());
        getPrizes().then((data) => {
            dispatch(getPrizesSuccess(data.data));
        }, (err) => {
            dispatch(getPrizesFailed(err));
        })
    }
}

function getPrizesStart() {
    return {
        type: 'GET_PRIZES_START'
    };
}

function getPrizesSuccess(prizes) {
    return {
        type: 'GET_PRIZES_SUCCESS',
        payload: {
            prizes
        }
    }
}

function getPrizesFailed(error) {
    return {
        type: 'GET_PRIZES_ERROR',
        payload: {
            error
        }
    };
}

export function buyPrizeAction(product, user) {
    return dispatch => {
        dispatch(buyPrizeStart(product, user));
        buyPrize(product.id, user.token).then((data) => {
            setTimeout(() => {
                dispatch(buyPrizeSuccess(product, user, data.data.product_key));
            }, 2000);

        }, (err) => {
            dispatch(buyPrizeFailed(product, user, err));
        })
    }
}

function buyPrizeStart(product, user) {
    return {
        type: 'BUY_PRIZE_START',
        payload: {
            product, user
        }
    };
}

function buyPrizeSuccess(product, user, productKey) {
    return {
        type: 'BUY_PRIZE_SUCCESS',
        payload: {
            product, user, productKey
        }
    }
}

function buyPrizeFailed(product, user, error) {
    return {
        type: 'BUY_PRIZE_FAILED',
        payload: {
            product, user, error
        }
    };
}
