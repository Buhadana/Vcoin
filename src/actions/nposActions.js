import { getNpos } from '../api/npoApi'

export const populateNpos = () => {
    let onError = error => {
        console.log(error);
    };
    return dispatch => {
        getNpos()
            .then(result => {
                dispatch(receivedNpos(result.data));
            })
            .catch(onError);
    }
};

export const receivedNpos = npos => ({
    type: 'RECEIVED_NPOS',
    payload: {
        npos
    }
});
