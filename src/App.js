import React, { Component } from 'react';
import './App.css';
import NavBar from './navbar';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { StorePage } from './pages/store';
import { VolunteersPage } from './pages/volunteers';
import { connect } from 'react-redux';
import { populateNpos } from './actions/nposActions';
import { CreateVolunteer } from './pages/create-volunteer';
import VolunteeringManagementPage  from './pages/volunteeringManagement';

class App extends Component {
  componentDidMount() {
    let { populateNpos } = this.props;
    populateNpos();
  }
  render() {
      let { npos } = this.props;
      return (
      <Router>
        <div className="App">
          <NavBar />
          <div className="content">
            <Route exact path="/" render={props => <VolunteersPage npos={npos} />} />
            <Route path="/store" component={StorePage} />
            <Route path="/create" component={CreateVolunteer} />
            <Route exact path="/management" component={VolunteeringManagementPage} />
          </div>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  npos: state.npos
});

const mapDispatchToProps = dispatch => ({
  populateNpos: () => {
    dispatch(populateNpos());
  }
});

App = connect(mapStateToProps, mapDispatchToProps)(App);

export default App;