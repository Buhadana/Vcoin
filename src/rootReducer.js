import { combineReducers } from 'redux';
import prize from './reducers/prize';
import user from './reducers/user';
import npos from './reducers/npos';
import selectedVolunteering from './reducers/selectedVolunteering';
import volunteering from './reducers/volunteering';
import createVolunteer from './reducers/create-volunteer';

const rootReducer = combineReducers({
    prize,
    user,
    npos,
    volunteering,
    createVolunteer,
    selectedVolunteering
});

export default rootReducer;