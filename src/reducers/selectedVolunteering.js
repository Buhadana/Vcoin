/**
 * Created by doria on 8/25/2017.
 */
export default function selectedVolunteering(state = {}, action) {
    switch (action.type) {
        case 'SELECT_NPO':
            return {
                selectedNpo: action.id,
                selectedVolunteering: undefined
            };
        case 'SELECT_VOLUNTEERING':
            return {
                ...state,
                selectedVolunteering: action.id
            };
        default:
            return {
                selectedNpo: undefined,
                selectedVolunteering: undefined
            };
    }
}