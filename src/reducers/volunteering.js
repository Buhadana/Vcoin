export default function user(state = { registered: false }, action) {
    switch(action.type){
        case 'SET_REGISTERED':
            return {
                registered: action.payload.registered
            };
        default:
            return state;
    }
}