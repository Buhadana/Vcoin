export default function createVolunteer(state = {
    loading: false
}, action) {
    switch (action.type) {
        case 'CREATE_VOLUNTEERING_START':
            return {
                ...state,
                loading: true
            };
        case 'CREATE_VOLUNTEERING_SUCCESS':
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
}
