export default function prize(state = { prizes: [], productBuy: undefined }, action) {

    switch (action.type) {
        case 'GET_PRIZES_SUCCESS':
            return {
                ...state,
                prizes: action.payload.prizes
            };
        case 'BUY_PRIZE_START':
            return {
                ...state,
                productBuy: {
                    product: action.payload.product,
                    loading: true
                }
            };
        case 'BUY_PRIZE_SUCCESS':
            return {
                ...state,
                productBuy: {
                    product: action.payload.product,
                    loading: false,
                    productKey: action.payload.productKey
                }
            };
        default:
            return state;
    }
}