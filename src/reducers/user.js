export default function user(state = {}, action) {
    switch (action.type) {
        case 'RECEIVED_USER':
            let { id, name, fb_token, image_url, vcoins } = action.data.data;
            return {
                id,
                name,
                token: fb_token,
                pictureUrl: image_url,
                vcoins
            };
        case 'BUY_PRIZE_SUCCESS':
            return {
                ...state,
                vcoins: state.vcoins - action.payload.product.cost
            };
        case 'ADD_VCOINS':
            return {
                ...state,
                vcoins: state.vcoins + action.add
            };
        default:
            return state;
    }
}