export default function npos(state = [], action) {
    switch (action.type) {
        case 'RECEIVED_NPOS':
            return action.payload.npos;
        case 'CREATE_VOLUNTEERING_SUCCESS':
            action.payload.data.users = [];
            return state.map((npo) => {
                if (npo.id === action.payload.data.npo_id) {
                    return {
                        ...npo,
                        volunteerings: [...npo.volunteerings, action.payload.data]
                    }
                }
                return npo;
            })

        default:
            return state;
    }
}