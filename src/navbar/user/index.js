import * as React from 'react';
import './style.css';

export class UserInfo extends React.Component {
    render() {
        return <div className="user-info">
            <div className="user-vcoins">
                <div className="coinLogo"><img src="vCoin.png" alt="vcoin" title="vcoin" /></div>
                <div className="coins">{this.props.user.vcoins}</div>
            </div>
            <div className="spacer"></div>

            <div className="user-name">
                <span>{this.props.user.name}</span>
            </div>
            <div className="user-pic">
                <img src={this.props.user.pictureUrl} />
            </div>

        </div>
    }
}