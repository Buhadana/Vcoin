import * as React from 'react';
import { UserInfo } from './user';
import './style.css';
import { Link } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login';
import { connect } from 'react-redux'
import { sendRegisterUser } from '../actions/userActions'

class NavBar extends React.Component {
    render() {
        return <div className="navbar">
            <div className="left-side">
                <img src="/v.png" className="logo" title="vcoin" alt="vcoin" />
                <div className="links">
                    <Link to="/">
                        <div className="link">
                            <div className="reset">התנדבויות</div>
                        </div>
                    </Link>
                    <Link to="/store">
                        <div className="link">
                            <div className="reset">חנות</div>
                        </div>
                    </Link>
                    <Link to="/create">
                        <div className="link">
                            <div className="reset">צור התנדבות</div>
                        </div>
                    </Link>
                    <Link to="/management">
                        <div className="link">
                            <div className="reset">נהל התנדבויות</div>
                        </div>
                    </Link>
                </div>
            </div>
            {this.props.userIsLogged ? <UserInfo user={this.props.user} /> : <FacebookLogin appId="898507293646513" onClick={this.componentClicked} autoLoad={localStorage.getItem('fbAutoLoad') !== null} fields="name,email,picture" callback={this.props.fbSetUser} size="small" icon="fa-facebook" />}
        </div>
    }
}

const mapStateToProps = (state) => ({
    userIsLogged: state.user.token !== undefined,
    user: state.user
});

const mapDispatchToProps = (dispatch) => ({
    fbSetUser: (facebookResult) => {
        dispatch(sendRegisterUser(facebookResult.name, facebookResult.id, facebookResult.picture.data.url))
        localStorage.setItem('fbAutoLoad', true);
    }
});

NavBar = connect(mapStateToProps, mapDispatchToProps)(NavBar);
export default NavBar