import * as React from 'react';
import './styles.css';
import { connect } from 'react-redux';
import { createVolunteeringAction } from '../../actions/volunteeringActions'
import * as LinkedStateMixin from 'react-addons-linked-state-mixin';
import { Loader } from '../../components/loader'


class CreateVolunteerContainer extends React.Component {
    mixins = [LinkedStateMixin];
    constructor() {
        super();
        this.createValueLink = this.createValueLink.bind(this);
        this.save = this.save.bind(this);
        this.defaultState = {
            title: "",
            desctiption: "",
            npo_id: 1,
            min_users: "",
            max_users: "",
            vcoins_cost: "",
            date: "",
            hours: "",
            location: ""
        };
        this.state = { ...this.defaultState }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.loading === false && this.props.loading === true) {
            this.setState({
                ...this.defaultState
            });
        }
    }

    handleChange(prop) {
        return (newValue) => {
            this.setState({ [prop]: newValue });
        }
    }

    createValueLink(prop) {
        return {
            value: this.state[prop],
            requestChange: this.handleChange(prop).bind(this)
        }
    }

    save() {
        let volunteer = {
            title: this.state.title,
            desctiption: this.state.desctiption,
            npo_id: parseInt(this.state.npo_id),
            min_users: this.state.min_users,
            max_users: this.state.max_users,
            vcoins_cost: this.state.vcoins_cost,
            date: this.state.date,
            hours: this.state.hours,
            location: this.state.location
        };

        this.props.createVolungteering(volunteer);
    }

    render() {
        return <div className="create-valunteering">
            {(!this.props.loading) ?
                <div>
                    <div className="input">
                        כותרת:<input type="text" valueLink={this.createValueLink('title')} />
                    </div>

                    <div className="input">
                        ארגון:
                <select valueLink={this.createValueLink('npo_id')}>
                            {this.props.npos.map((npo, i) => (<option key={i} value={npo.id}>{npo.name}</option>))}
                        </select>
                    </div>

                    <div className="input">
                        תיאור:<input type="text" valueLink={this.createValueLink('desctiption')} />
                    </div>

                    <div className="input">
                        כמות מתנדבים מינימאלית:<input type="text" valueLink={this.createValueLink('min_users')} />
                    </div>

                    <div className="input">
                        כמות מתנדבים מקסימאלית:<input type="text" valueLink={this.createValueLink('max_users')} />
                    </div>

                    <div className="input">
                        תמורה:<input type="text" valueLink={this.createValueLink('vcoins_cost')} />
                    </div>

                    <div className="input">
                        תאריך:<input type="text" valueLink={this.createValueLink('date')} />
                    </div>

                    <div className="input">
                        כמות שעות:<input type="text" valueLink={this.createValueLink('hours')} />
                    </div>

                    <div className="input">
                        מיקום:<input type="text" valueLink={this.createValueLink('location')} />
                    </div>

                    <div className="save-button" onClick={this.save}>שמירה</div>

                </div> :
                <Loader></Loader>
            }
        </div>
    }

}


const mapStateToProps = (state) => {
    return {
        npos: state.npos,
        loading: state.createVolunteer.loading
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        createVolungteering: (volunteering) => { createVolunteeringAction(volunteering)(dispatch) }
    }
}

export const CreateVolunteer = connect(mapStateToProps, mapDispatchToProps)(CreateVolunteerContainer);
