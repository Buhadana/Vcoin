import * as React from 'react';

import './style.css';

export default class Volunteering extends React.Component {

    renderDate(date) {
        const options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        return new Date(date).toLocaleTimeString('he-IL', options);
    }

    render() {
        const { volunteering } = this.props;
        if (volunteering) {
            return <div className="volunteering-container">
                <b>{volunteering.title}</b>
                <p> תתקיים ב{this.renderDate(volunteering.date)}</p>
                <p>מיקום: {volunteering.location}</p>
            </div>;
        }
        return null;
    }
}