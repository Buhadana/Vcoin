import * as React from 'react';

import './style.css';

const User = ({user}) => {
    return <div className="single-user">
        <img className="user-img" src={user.image_url}/>
        <b className="user-name">{user.name}</b>
    </div>;
};

const Users = ({users}) => {
    return <div className="users">
        <b>{users.length === 0 ? 'אין מתנדבים רשומים להתנדבות זו' : 'מתנדבים רשומים:'}</b>
        {
            users.map(user =>
                <User user={user} key={user.id} />
            )
        }
    </div>;
};

export default Users;