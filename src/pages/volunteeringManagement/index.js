import * as React from 'react';
import Dropdown from 'react-dropdown'
import './style.css';
import Users from './user'
import {connect} from 'react-redux'
import {selectNpo, selectVolunteering} from '../../actions/selectedVolunteering'
import Volunteering from "./volunteering";

class VolunteeringManagementPage extends React.Component {
    findQueryByValue(searchId) {
        return ((v) => v.value === searchId);
    }

    findQueryById(searchId) {
        return ((v) => v.id === searchId);
    }

    render() {
        let {selectedNpo, selectedVolunteering, npos} = this.props;
        let nposDropdown = npos.map((v) => ({value: v.id, label: v.name}));
        let volunteeringDropdown = selectedNpo !== undefined ? npos.find(this.findQueryById(selectedNpo)).volunteerings.map((v) => ({
            value: v.id,
            label: v.title
        })) : [];
        let users = selectedVolunteering !== undefined ? npos.find(this.findQueryById(selectedNpo)).volunteerings.find(this.findQueryById(selectedVolunteering)).users : [];
        return <div className="management">
            <Dropdown options={nposDropdown} value={nposDropdown.find(this.findQueryByValue(selectedNpo))}
                      onChange={this.props.selectNpo} placeholder="בחר עמותה"/>
            <Dropdown options={volunteeringDropdown}
                      value={volunteeringDropdown.find(this.findQueryByValue(selectedVolunteering))}
                      onChange={this.props.selectVolunteering} placeholder="בחר התנדבות"/>
            {
                selectedVolunteering !== undefined &&
                <div className="volunteering-and-users">
                    <Volunteering
                        volunteering={npos.find(this.findQueryById(selectedNpo)).volunteerings.find(this.findQueryById(selectedVolunteering))}/>
                    <Users users={users}/>
                </div>
            }
        </div>
    }
}

const mapStateToProps = (state) => ({
    npos: state.npos,
    selectedNpo: state.selectedVolunteering.selectedNpo,
    selectedVolunteering: state.selectedVolunteering.selectedVolunteering
});

const mapDispathToProps = (dispatch) => ({
    selectNpo: (res) => {
        dispatch(selectNpo(res.value))
    },
    selectVolunteering: (res) => {
        dispatch(selectVolunteering(res.value))
    }
});

VolunteeringManagementPage = connect(mapStateToProps, mapDispathToProps)(VolunteeringManagementPage);
export default VolunteeringManagementPage