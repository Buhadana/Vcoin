import * as React from 'react';
import { getPrizesAction, buyPrizeAction } from '../../actions/prizeActions.js'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import './style.css';

import { Loader } from '../../components/loader'

class StorePageContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            showNotEnough: false,
            product: undefined,
            showProductBuy: false
        }
        this.buy = this.buy.bind(this);
        this.hideNotEnoghMoney = this.hideNotEnoghMoney.bind(this);
        this.hideProductBought = this.hideProductBought.bind(this);
    }

    componentDidMount() {
        this.props.getPrizes();
    }

    buy(product) {
        return event => {
            if (product.cost > this.props.user.vcoins) {
                this.showNotEnoghMoney(product);
            }
            else {
                this.props.buyProduct(product, this.props.user);
            }
        }
    }

    showNotEnoghMoney(product) {
        this.setState({
            showNotEnough: true,
            product
        });
    }

    hideNotEnoghMoney() {
        this.setState({
            showNotEnough: false
        });
    }

    hideProductBought() {
        this.setState({
            showProductBuy: false
        })
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props || this.props.productBuy !== nextProps.productBuy) {
            this.setState({
                showProductBuy: true,
                productBuy: nextProps.productBuy
            });
        }
    }

    render() {
        return <div className="store">
            {this.state.showProductBuy ?
                <div className="modal">
                    <div className="not-enough buying">
                        {this.state.productBuy.loading ? undefined : <div className="x" onClick={this.hideProductBought}>X</div>}
                        <header>{this.state.productBuy.loading ? this.state.productBuy.product.name : 'מזל טוב!'}</header>
                        {this.state.productBuy.loading ? <div><Loader></Loader></div> :
                            <div className="product-key-container">
                                <div className="product-key-header">
                                    קוד המוצר שלך
                                </div>
                                <div className="product-key">
                                    {this.state.productBuy.productKey}
                                </div>
                            </div>
                        }
                    </div>
                </div> :
                null
            }
            {this.state.showNotEnough ? <div className="modal">
                <div className="not-enough">
                    <div className="x" onClick={this.hideNotEnoghMoney}>X</div>
                    <header dir="rtl">לא צברת מספיק vCoins</header>
                    <div dir="rtl" className="not-enough-content">המוצר עולה  {this.state.product.cost} אך לך יש רק {this.props.user.vcoins} vCoins</div>
                    <div className="vbutton"><Link to="/"><div className="volunteer-button">עבור להתנדבות</div></Link></div>
                </div>
            </div> : null}
            <div className="products">
                {
                    this.props.prizes.map((product, i) => (
                        <div className="product" key={i}>
                            <div className="product-image"><img src={product.image_url} alt="product" title="product" /></div>
                            <div className="product-info">
                                <div className="header">{product.name}</div>
                                <div className="description">{product.desctiption}</div>
                            </div>

                            {this.props.user.token ? <div className="buy-button" onClick={this.buy(product)}>
                                <div className="inner-button">
                                    <div className="cost"><span className="price">{product.cost}</span><span><img src="/vCoin.png" alt="vcoin" title="vcoin" /></span></div>
                                    <div>קנה</div>
                                </div>
                            </div> : null}
                        </div>
                    ))
                }
            </div>
        </div>
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        prizes: state.prize.prizes,
        productBuy: state.prize.productBuy
    };
}

function mapActionsToProps(dispatch) {
    return {
        getPrizes: () => { getPrizesAction()(dispatch); },
        buyProduct: (product, user) => { buyPrizeAction(product, user)(dispatch); }
    }
}

export const StorePage = connect(mapStateToProps, mapActionsToProps)(StorePageContainer)