import * as React from 'react';

import './style.css';

import { NpoMenuItem } from '../npo-menu-item';

const NpoMenu = ({ npos, onVolunteeringClick }) => (
    <div className="npo-menu">
        <h1 className="npo-menu-title">אירגונים</h1>
        {npos.map((npo, index) => (
            <NpoMenuItem npo={npo} key={index} onVolunteeringClick={volunteerIndex => {
                onVolunteeringClick(index, volunteerIndex);
            }}></NpoMenuItem>
        ))}
    </div>
);

export default NpoMenu;