import * as React from 'react';

import './style.css';

import Npo from './npo';
import NpoMenu from './npo-menu';
import { Volunteer } from './volunteer';
import { connect } from 'react-redux';
import { register } from '../../actions/volunteeringActions';
import {addVcoins} from '../../actions/userActions'

export class VolunteersPage extends React.Component {
    constructor() {
        super();
        this.state = {
            npoIndex: 0,
            volunteerIndex: 0
        };
    }
    renderCurrentNpo() {
        const { npos, register, userFbToken } = this.props;
        const { npoIndex, volunteerIndex } = this.state;
        if (npos.length > 0) {
            const npo = npos[npoIndex];
            const volunteer = npo.volunteerings[volunteerIndex];
            const isAlreadyRegistered = this.isAlreadyRegistered(volunteer);
            return <div>
                <div id="left" className="panel"
                    style={{ backgroundImage: `url(${npo.image_url})` }}>
                   <NpoMenu npos={npos} onVolunteeringClick={(npoIndex, volunteerIndex) => {
                       this.setState({
                           npoIndex,
                           volunteerIndex
                       });
                    }}></NpoMenu>
                </div>
                <div id="middle" className="panel">
                    <Npo npo={npo}></Npo>
                </div>
                <div id="right" className="panel">
                    <Volunteer volunteer={volunteer} isRegistered={isAlreadyRegistered} isLoggedIn={userFbToken} onRegister={() => {
                        if (!isAlreadyRegistered && userFbToken) {
                            register(volunteer.id, userFbToken, volunteer.vcoins_cost);
                        }
                    }}></Volunteer>
                </div>
            </div>;
        }
        return null;
    }
    isAlreadyRegistered(volunteer) {
        const { isRegistered } = this.props;
        return isRegistered || this.isUserAlreadyRegisteredToVolunteer(volunteer);
    }
    isUserAlreadyRegisteredToVolunteer(volunteer) {
        const { userId } = this.props;
        return volunteer.users.find(user => {
            return user.id === userId;
        });
    }
    render() {
        return <div className="volunteers">
            {this.renderCurrentNpo()}
        </div>;
    }
}

const mapStateToProps = (state) => ({
    userId: state.user.id,
    userFbToken: state.user.token,
    isRegistered: state.volunteering.registered
});

const mapDispatchToProps = dispatch => ({
    register: (volunteeringId, userFbToken, vcoins) => {
        dispatch(register(volunteeringId, userFbToken));
        dispatch(addVcoins(vcoins))
    }
});

VolunteersPage = connect(mapStateToProps, mapDispatchToProps)(VolunteersPage);
export default VolunteersPage;