import * as React from 'react';

import './style.css';

const Npo = ({npo}) => (
    <div className="npo">
       <h1 className="npo-name">{npo.name}</h1>
       <p className="npo-description">
           {npo.desctiption}
       </p>
    </div>
);

export default Npo;