import * as React from 'react';

import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import withScriptjs from 'react-google-maps/lib/async/withScriptjs';

const AsyncGoogleMap = withScriptjs(
    withGoogleMap(
        props => (
            <GoogleMap
                ref={props.onMapLoad}
                defaultZoom={15}
                {...props}
                onClick={props.onMapClick}
            >
                {props.markers.map((marker, index) => (
                    <Marker
                        key={index}
                        {...marker}
                        onRightClick={() => props.onMarkerRightClick(marker)}
                    />
                ))}
            </GoogleMap>
        )
    )
);

export default AsyncGoogleMap;