import * as React from 'react';

import './style.css';

import AsyncGoogleMap from './map';
import config from '../../../config';

export class Volunteer extends React.Component {
    constructor() {
        super();
        this.state = {
            markers: []
        };
    }
    componentDidMount() {
        const { volunteer } = this.props;
        this.getMarkersByAddress(volunteer.location);
    }
    componentWillReceiveProps(nextProps) {
        const { volunteer } = this.props;
        if (volunteer.id !== nextProps.volunteer.id) {
            this.getMarkersByAddress(nextProps.volunteer.location);
        }
    }
    getMarkersByAddress(address) {
        const geocoder = new window.google.maps.Geocoder();
        geocoder.geocode({
            address
        }, (results, status) => {
            if (status === window.google.maps.GeocoderStatus.OK) {
                let marker = results[0].geometry.location;
                this.setState({
                    markers: [{
                        position: {
                            lat: marker.lat(),
                            lng: marker.lng()
                        },
                        defaultAnimation: 2
                    }]
                });
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
    renderGoogleMap() {
        const { markers } = this.state;
        if (markers.length > 0) {
            let markerPosition = markers[0].position;
            return <AsyncGoogleMap
                googleMapURL={config.googleMapUrl}
                loadingElement={
                    <div style={{ height: `100%` }}>
                        Loading Map...
                </div>
                }
                containerElement={
                    <div style={{ bottom: `0%`, height: `80%`, position: `relative` }} />
                }
                mapElement={
                    <div style={{ bottom: `0%`, height: `80%`, position: `relative` }} />
                }
                onMapLoad={() => { }}
                onMapClick={() => { }}
                center={markerPosition}
                defaultCenter={markerPosition}
                markers={markers}
                onMarkerRightClick={() => { }}
            />;
        }
    }
    renderRegisterMessage() {
        const { isRegistered, isLoggedIn } = this.props;
        if (!isLoggedIn) {
            return 'התחבר';
        }
        if (isRegistered) {
            return 'רשום';
        }
        return 'לחץ להרשמה';
    }
    renderDate(volunteer) {
        const options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        return new Date(volunteer.date).toLocaleTimeString('he-IL', options);
    }
    render() {
        const { volunteer, onRegister, isRegistered, isLoggedIn } = this.props;
        return <div className="volunteer">
            <h1 className="volunteer-title">{volunteer.title}</h1>
            <p className="volunteer-description">
                {volunteer.desctiption}
            </p>
            <div className="info-title">{this.renderDate(volunteer)}</div>
            <div className="info-title" dir="rtl">{volunteer.hours} שעות</div>
            <div className="info-title" dir="rtl">
                {volunteer.vcoins_cost}
                <img className="vcoin" src="vCoin.png" alt="vcoin" title="vcoin" />
            </div>
            <div className="info-title">מיקום</div>
            {this.renderGoogleMap()}
            <div className={`register-to-volunteer ${isRegistered || !isLoggedIn ? 'disabled' : 'enabled'}`} onClick={(onRegister)}>
                {this.renderRegisterMessage()}
            </div>
        </div>;
    }
}
