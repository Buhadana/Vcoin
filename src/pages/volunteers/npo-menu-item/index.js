import * as React from 'react';

import './style.css';

export class NpoMenuItem extends React.Component {
    constructor() {
        super();
        this.state = {
            opened: false
        };
    }
    onNpoClick() {
        const { opened } = this.state;
        this.setState({
            opened: !opened
        });
    }
    renderVolunteers() {
        const { opened } = this.state;
        if (opened) {
            const { npo, onVolunteeringClick } = this.props;
            return <div className="npo-menu-item-volunteers">
                {npo.volunteerings.map((volunteering, index) => (
                    <div className="npo-menu-item-volunteer" onClick={(e) => {
                        onVolunteeringClick(index);
                    }}>{volunteering.title}</div>
                ))}
            </div>;
        }
        return null;
    }
    render() {
        const { npo } = this.props;
        return <div className="npo-menu-item" onClick={this.onNpoClick.bind(this)}>
            <h1 className="npo-menu-item-name">{npo.name}</h1>
            {this.renderVolunteers()}
        </div>
    }
}